import { FETCH_DECODE_SUCCESS, FETCH_ENCODE_SUCCESS } from "../actionTypes";

const initialState = {
  encode: "",
  password: "",
  decode: ""
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ENCODE_SUCCESS:
      return {...state, encode: action.encode}
    case FETCH_DECODE_SUCCESS:
      return {...state, decode: action.decode}
    case "ENCODE":
      return {...state, encode: action.encode}
    case "DECODE":
      return {...state, decode: action.decode}
    case "PASSWORD": 
      return {...state, password: action.password}  
    default:
      return state;
  }
};

export default reducer;