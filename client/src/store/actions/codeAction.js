import axios from "../../axiosCode";
import { FETCH_DECODE_SUCCESS, FETCH_ENCODE_SUCCESS } from "../actionTypes";


const fetchEncodeSuccess = (encode) => {
  return {type: FETCH_ENCODE_SUCCESS, encode}
};

const fetchDecodeSuccess = (decode) => {
  return {type: FETCH_DECODE_SUCCESS, decode}
};

export const encodeOnChange = (encode) => {
  return {type: "ENCODE", encode}
};

export const decodeOnChange = (decode) => {
  return {type: "DECODE", decode}
};

export const passwordOnChange = (password) => {
  return {type: "PASSWORD", password}
};

export const fetchDecode = (codeData) => {
  return async dispatch => {
    try {
      const response = await axios.post("/encode", codeData);
      dispatch(fetchDecodeSuccess(response.data.encode));
    } catch (e) {
      console.log(e);
    }
  };
};

export const fetchEncode = (codeData) => {
  return async dispatch => {
    try {
      const response = await axios.post("/decode", codeData);
      dispatch(fetchEncodeSuccess(response.data.decode));
    } catch (e) {
      console.log(e);
    }
  };
};