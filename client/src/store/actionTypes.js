export const FETCH_ENCODE_SUCCESS = "FETCH_ENCODE_SUCCESS";
export const FETCH_DECODE_SUCCESS = "FETCH_DECODE_SUCCESS";

export const ENCODE = "ENCODE";
export const DECODE = "DECODE";
export const PASSWORD = "PASSWORD";