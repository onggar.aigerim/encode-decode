import { useDispatch, useSelector } from "react-redux";
import CodeForm from "../../components/CodeForm/CodeForm";
import { decodeOnChange, encodeOnChange, fetchDecode, fetchEncode, passwordOnChange } from "../../store/actions/codeAction";

const Code = () => {
  const dispatch = useDispatch();

  const decode = useSelector(state => state.code.decode);
  const encode = useSelector(state => state.code.encode);
  const password = useSelector(state => state.code.password);

  const encodeChange = (e) => dispatch(encodeOnChange(e.target.value));
  const decodeChange = (e) => dispatch(decodeOnChange(e.target.value));
  const passwordChange = (e) => dispatch(passwordOnChange(e.target.value));

  const decodeResult = () => {
    if (password !== "") {
      dispatch(fetchDecode({password: password, message: encode}));
      dispatch(passwordOnChange(""));
    } else {
      dispatch(passwordOnChange("Password required!!!"));
    };
  };

  const encodeResult = () => {
    if (password !== "") {
      dispatch(fetchEncode({password: password, message: decode}));
      dispatch(passwordOnChange(""));
    } else {
      dispatch(passwordOnChange("Password required!!!"));
    };
  };

  return <CodeForm
    encode={encode}
    decode={decode}
    password={password}
    encodeChange={encodeChange}
    decodeChange={decodeChange}
    passwordChange={passwordChange}
    encodeResult={encodeResult}
    decodeResult={decodeResult}
  />
};

export default Code;