import styled from "styled-components";

const TextContainer = styled.div`
  display: flex;
  width: 471px;
  justify-content: space-between;
  align-items: center;
  margin: 20px auto;
  & label {
    font-size: 20px;
  }
  & textarea {
    width: 289px;
    height: 100px;
    margin: 10px 0;
    font-size: 20px;
  }
  & button {
    border: none;
    background: none;
    cursor: pointer;
    font-size: 20px;
    margin: 0 5px;
  }
  & input {
    font-size: 20px;
  }
`;

const CodeForm = ({
  encode, 
  decode,
  password,
  encodeChange,
  decodeChange,
  passwordChange,
  decodeResult, 
  encodeResult
}) => {
  return <div>
    <TextContainer>
      <label>Decoded message</label>
      <textarea 
        value={encode}
        onChange={encodeChange}
      />
    </TextContainer>
    <TextContainer>
      <label>Password</label>
      <span>
        <input 
          value={password}
          onChange={passwordChange}
        />
        <button
          onClick={decodeResult}
        >
          &#8659;
        </button>
        <button
          onClick={encodeResult}
        >
          &#8657;
        </button>
      </span>
    </TextContainer>
    <TextContainer>
      <label>Encoded message</label>
      <textarea 
        value={decode}
        onChange={decodeChange}
      />
    </TextContainer>  
  </div>
};

export default CodeForm;