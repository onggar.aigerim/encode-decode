import ReactDOM from 'react-dom/client';
import { configureStore } from "@reduxjs/toolkit";
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import "./index.css";
import App from './App';
import codeReducer from '../src/store/reducers/codeReducer';

const store = configureStore({reducer: {
  code: codeReducer
}});

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(app);