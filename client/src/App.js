import Code from "./containers/Code/Code";

function App() {
  return (
    <div className="App">
      <Code/>
    </div>
  );
}

export default App;
