const express = require("express");
const cors = require("cors");
const Vigenere = require("caesar-salad").Vigenere;

const app = express();
const PORT = 8000;

app.use(cors());
app.use(express.json());

app.post("/encode", (request, response) => {
  const password = request.body.password;
  const message = request.body.message;
  response.send({encode: Vigenere.Cipher(password).crypt(message)});
});

app.post("/decode", (request, response) => {
  const password = request.body.password;
  const message = request.body.message;
  response.send({decode: Vigenere.Decipher(password).crypt(message)});
});

app.listen(PORT, () => {
  console.log(`Server running at http://localhost:${PORT}`);
});